Usage:

from retinaface_openface import Extractor

# Target_size resizes any image to contain ~ target_size**2 pixels.
# Smaller target_size improves speed and decreases accuracy.
extractor = Extractor(target_size=300)

## Detect faces
boxes, landmarks = extractor.detect_faces(image)

## Encode faces into 128-d vectors. faces is a list of numpy arrays (images) 
vectors = extractor.embed_faces(faces)

## Detect and encode faces
boxes, landmarks, faces = extractor.extract_faces(image)