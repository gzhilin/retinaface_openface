import torch
import cv2
import numpy as np
from os.path import dirname

from .models.retinaface import RetinaFace
from .data import cfg_mnet, cfg_re50
from .layers.functions.prior_box import PriorBox
from .utils.box_utils import decode, decode_landm
from .utils.nms.py_cpu_nms import py_cpu_nms


curr_dir = dirname(__file__)

def check_keys(model, pretrained_state_dict):
    ckpt_keys = set(pretrained_state_dict.keys())
    model_keys = set(model.state_dict().keys())
    used_pretrained_keys = model_keys & ckpt_keys
    unused_pretrained_keys = ckpt_keys - model_keys
    missing_keys = model_keys - ckpt_keys
    assert len(used_pretrained_keys) > 0, 'load NONE from pretrained checkpoint'
    return True

def remove_prefix(state_dict, prefix):
    ''' Old style model is stored with all names of parameters sharing common prefix 'module.' '''
    f = lambda x: x.split(prefix, 1)[-1] if x.startswith(prefix) else x
    return {f(key): value for key, value in state_dict.items()}

def load_model(model, pretrained_path, load_to_cpu):
    if load_to_cpu:
        pretrained_dict = torch.load(pretrained_path, map_location=lambda storage, loc: storage)
    else:
        device = torch.cuda.current_device()
        pretrained_dict = torch.load(pretrained_path, map_location=lambda storage, loc: storage.cuda(device))
    if "state_dict" in pretrained_dict.keys():
        pretrained_dict = remove_prefix(pretrained_dict['state_dict'], 'module.')
    else:
        pretrained_dict = remove_prefix(pretrained_dict, 'module.')
    check_keys(model, pretrained_dict)
    model.load_state_dict(pretrained_dict, strict=False)
    return model

det_params = {
    'confidence_threshold': 0.6,
    'top_k': 5000,
    'nms_threshold': 0.4,
    'keep_top_k': 750,
}

class Extractor():
    def __init__(self, cpu=True, backbone='mnet', target_size=640,
        det_params=det_params):
        if backbone=='mnet':
            cfg = cfg_mnet
            rel_path = '/weights/mobilenet0.25_Final.pth'
        elif backbone=='re50':
            cfg = cfg_re50
            rel_path = '/weights/Resnet50_Final.pth'
        self.net = RetinaFace(cfg=cfg, phase = 'test')
        self.net = load_model(self.net, curr_dir + rel_path, cpu)
        self.net = self.net.eval()

        device = torch.device("cpu" if cpu else "cuda")

        self.scale = torch.Tensor([target_size, target_size])
        self.scale1 = self.scale.repeat(5)
        self.scale = self.scale.repeat(2)
        self.scale = self.scale.to(device)
        self.scale1 = self.scale1.to(device)

        with torch.no_grad():
            priorbox = PriorBox(cfg, image_size=(target_size,target_size))
            self.priors = priorbox.forward()
            self.priors = self.priors.to(device)

        self.target_size = (target_size, target_size)
        self.img_size = (target_size, target_size)
        self.cpu = cpu
        self.cfg = cfg

        self.embedder = cv2.dnn.readNetFromTorch(curr_dir + '/weights/openface_nn4.small2.v1.t7')


    def cuda(self):
        self.net = self.net.cuda()
        self.scale = self.scale.cuda()
        self.scale1 = self.scale1.cuda()
        self.priors = self.priors.cuda()

    def cpu(self):
        self.net = self.net.cpu()
        self.scale = self.scale.cpu()
        self.scale1 = self.scale1.cpu()
        self.priors = self.priors.cpu()

    def detect_faces(self, image):
        with torch.no_grad():
            image_size = image.shape[:2]
            device = torch.device("cpu" if self.cpu else "cuda")
            scale_factor = 1
            new_img_size = self.img_size
            if (image_size != self.target_size):
                im_pix = image_size[0]*image_size[1]
                tg_pix = self.target_size[0]*self.target_size[1]
                if im_pix > tg_pix:
                    scale_factor = np.sqrt(tg_pix / im_pix)
                new_img_size = (int(image_size[0]*scale_factor), int(image_size[1]*scale_factor))

            if new_img_size != self.img_size:
                self.img_size = new_img_size
                self.scale = torch.Tensor([self.img_size[1], self.img_size[0]])
                self.scale1 = self.scale.repeat(5)
                self.scale = self.scale.repeat(2)
                self.scale = self.scale.to(device)
                self.scale1 = self.scale1.to(device)

                priorbox = PriorBox(self.cfg, image_size=self.img_size)
                self.priors = priorbox.forward()
                self.priors = self.priors.to(device)                
            
            img = cv2.resize(image, (self.img_size[1], self.img_size[0]))
            img = np.float32(img)
            img = img.transpose(2, 0, 1)
            img = torch.from_numpy(img).unsqueeze(0)
            img = img.to(device)

            loc, conf, landms = self.net(img)

            boxes = decode(loc.squeeze(0), self.priors, self.cfg['variance'])
            boxes = boxes * self.scale
            boxes = boxes.cpu().numpy()

            scores = conf.squeeze(0)[:, 1].cpu().numpy()
            landms = decode_landm(landms.squeeze(0), self.priors, self.cfg['variance'])
            landms = landms * self.scale1
            landms = landms.cpu().numpy()

            inds = np.where(scores > det_params['confidence_threshold'])[0]
            boxes = boxes[inds]
            landms = landms[inds]
            scores = scores[inds]

            order = scores.argsort()[::-1][:det_params['top_k']]
            boxes = boxes[order]
            landms = landms[order]
            scores = scores[order]

            dets = np.hstack((boxes, scores[:, np.newaxis])).astype(np.float32, copy=False)
            keep = py_cpu_nms(dets, det_params['nms_threshold'])
            dets = dets[keep, :]
            landms = landms[keep]

            dets = dets[:det_params['keep_top_k'], :]
            landms = landms[:det_params['keep_top_k'], :]


            boxes = (dets[:,:4]/scale_factor).astype(int)
            boxes[boxes[:,0] < 0] = 0
            boxes[boxes[:,1] < 0] = 0
            boxes[boxes[:,2] >= image_size[1]] = image_size[1] - 1
            boxes[boxes[:,3] >= image_size[0]] = image_size[0] - 1
            inds = boxes[:,2] - boxes[:,0] > 20
            inds = np.logical_and(inds, boxes[:,3] - boxes[:,1] > 20)
            boxes = boxes[inds]
            lms = (landms[inds]/scale_factor).astype(int)
            return boxes, lms


    def embed_faces(self, faces):
        if len(faces) == 0:
            return []
        face_batch = []
        for face in faces:
            faceBlob = cv2.dnn.blobFromImage(face, 1.0 / 255,
                (96, 96), (0, 0, 0), swapRB=True, crop=False)
            face_batch.append(faceBlob)
        face_batch = np.concatenate(face_batch, axis=0)
        self.embedder.setInput(face_batch)
        face_vecs = self.embedder.forward()
        return face_vecs

    def extract_faces(self, image):
        boxes, lms = self.detect_faces(image)
        faces = [image[box[1]:box[3],box[0]:box[2],:] for box in boxes]
        face_vecs = self.embed_faces(faces)
        return boxes, lms, face_vecs

        

    
